package com.techprimers.springbatchexample1.batch;

import com.techprimers.springbatchexample1.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<User, User> {

    private static final Map<String, String> DEPT_NAMES =
            new HashMap<>();

    public Processor(){
        DEPT_NAMES.put("1","JpMorganChase");
        DEPT_NAMES.put("2","Gore");
        DEPT_NAMES.put("3","Fidelity");
    }

    @Override
    public User process(User user) throws Exception {
        String locationCode = user.getLocation();
        String location = DEPT_NAMES.get(locationCode);
        user.setLocation(location);
        return user;
    }
}
